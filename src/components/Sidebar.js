import React from 'react'
import { NavLink } from 'react-router-dom'
import * as BsIcons from "react-icons/bs";

const Sidebar = () => {
    return (
        <div>
        <aside className="main-sidebar sidebar-light-dark elevation-1" style={{backgroundColor: "#fff"}}>
            <span className="brand-link">
                <img src="dist/img/logo.jpg" alt="AdminLTE Logo" className="brand-image img-circle elevation-2" />
                <span className="brand-text font-weight-bold"> SysDev-Tracker</span>
            </span>
            <div className="sidebar">
                <nav className="mt-4">
                <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li className="nav-item">
                    <NavLink to="/Dashboard" className="nav-link">
                        <BsIcons.BsHouse className="nav-icon text-lg" />
                        <p>Dashboard</p>
                    </NavLink>
                    </li>

                    <li className="nav-item">
                    <NavLink to="/Logstat" className="nav-link">
                        <BsIcons.BsClock className="nav-icon text-lg" />
                        <p>Log Status</p>
                    </NavLink>
                    </li>
    
                    <li className="nav-item">
                    <NavLink to="/User" className="nav-link">
                        <BsIcons.BsTable className="nav-icon" />
                        <p> User </p>
                    </NavLink>
                    </li>

                    <li className="nav-item">
                    <NavLink to="/Profile" className="nav-link">
                        <BsIcons.BsPerson className="nav-icon" />
                        <p> Profile </p>
                    </NavLink>
                    </li>
    
                    
                    
                </ul>
                </nav>
            </div>
        </aside>
        </div>
      );
    }

export default Sidebar