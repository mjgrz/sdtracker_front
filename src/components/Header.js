import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom';
import * as BsIcons from "react-icons/bs";

const Header = ({ url_str, fetchsession }) => {
  const navigate = useNavigate();
  const [session, setSession] = useState();
  
  const checksession = async() => {
    fetch(`${url_str}/checksession`,{credentials:"include"})
    .then((response) => response.json())
    .then((responseJson) => {
      const res = responseJson;
      console.log(res);
      fetchsession(res);
      setSession(res);
    });
  }

  const logout = async() => {
    fetch(`${url_str}/logout`,{credentials:"include"})
    .then((response) => response.json())
    .then((responseJson) => {
      if (responseJson.responsecode === "0934") {
        return  navigate("/");
      } 
      else {
        // Nothing to do
      }
    })
    .catch((error) => {
      console.log(error)
    });
  }
  
  useEffect(() => {
    checksession()
  }, [])
  
  return (
    <div>
      <nav className="main-header navbar navbar-expand navbar-white navbar-light justify-content-between">
        <ul className="navbar-nav">
          <li className="nav-item">
          <button className="nav-link" data-widget="pushmenu" href="#" style={{backgroundColor: "transparent", border: "none"}}><BsIcons.BsList className='text-lg'/></button>
          </li>
          <li className="nav-item">
          <span className="nav-link" style={{backgroundColor: "transparent", border: "none"}}>
            <strong>Hello! {session?session.fullname:''}</strong>
          </span>
          </li>
        </ul>
        <ul className="navbar-nav">
          <li className="nav-item">
          <span className="nav-link" style={{backgroundColor: "transparent", border: "none", cursor:'pointer'}} onClick={logout}>
            <strong>LOGOUT</strong>
          </span>
          </li>
        </ul>
      </nav>
    </div>
  )
}

export default Header