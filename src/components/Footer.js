const Footer = () => {
  return (
    <div>
      
      <footer className="main-footer">
        <strong>Copyright © 2024 <a href="https://nha.gov.ph/"> National Housing Authority</a>. </strong>
        All rights reserved.
        <div className="float-right d-none d-sm-inline-block">
          
        </div>
      </footer>

    </div>
  )
}

export default Footer