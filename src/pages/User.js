import { useState, useEffect } from 'react'
import Header from "../components/Header";
import Sidebar from "../components/Sidebar";
import Footer from '../components/Footer'
import { colorOpt } from '../datagrp/DataGrp';
import Pagination from './Pagination'

const User = ({ url_str, url_origin }) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('Accept', 'application/json');
  headers.append('Origin', `${url_origin}`);
  
  // PAGINATION SETTINGS
  const [totalPages, setTotalPages] = useState(0);
  const no_of_records_per_page = 10;
  const [rowCount, setRowCount] = useState(0);
  const [totalrowCount, setTotalRowCount] = useState(0);
  const [tblIndex, setTblIndex] = useState(0);
  // PAGINATION SETTINGS
  
  // SEARCH
  const [search, setSearch] = useState("");
  // SEARCH
  
  // FETCH USER DATA
  const execSearch = async() => {
    document.getElementById("loading").hidden = false;
    if(search == ""){
      const response = await fetch(`${url_str}/userscount`, { 
        mode: "cors",
        method: "POST",
        headers: headers,
        credentials: "include",
        body: JSON.stringify({
        }),
      });
      const data = await response.json();
      setTotalRowCount(data.rows[0].count)
      setTotalPages(Math.ceil(data.rows[0].count / no_of_records_per_page))
      window.setTimeout(() => {
        let number = 1
        fetchdata(number)
      }, 500)
    }
    else if (search != ""){
      const response = await fetch(`${url_str}/userscountlike`, { 
        mode: "cors",
        method: "POST",
        headers: headers,
        credentials: "include",
        body: JSON.stringify({
          item: search,
        }),
      });
      const data = await response.json();
      setTotalRowCount(data.rows[0].count)
      setTotalPages(Math.ceil(data.rows[0].count / no_of_records_per_page))
      window.setTimeout(() => {
        let number = 1
        fetchdata(number)
      }, 500)
    }
  }

  const [userdata, setUserData] = useState([]);
  const fetchdata = async(number) => {
    document.getElementById("loading").hidden = false;
    if(search == ""){
      const response = await fetch(`${url_str}/usersset1`, { 
        mode: "cors",
        method: "POST",
        headers: headers,
        credentials: "include",
        body: JSON.stringify({
          offset: (number - 1) * no_of_records_per_page,
          limit: no_of_records_per_page,
        }),
      });
      const data = await response.json();
      setUserData(data.rows)
      setRowCount(data.rowCount)
      setTblIndex((number-1)*no_of_records_per_page)
      window.setTimeout(() => {
          document.getElementById("loading").hidden = true;
      }, 500)
    }
    else if(search != ""){
      const response = await fetch(`${url_str}/usersset2`, { 
        mode: "cors",
        method: "POST",
        headers: headers,
        credentials: "include",
        body: JSON.stringify({
          offset: (number - 1) * no_of_records_per_page,
          limit: no_of_records_per_page,
          item: search,
        }),
      });
      const data = await response.json();
      setUserData(data.rows)
      setRowCount(data.rowCount)
      setTblIndex((number-1)*no_of_records_per_page)
      window.setTimeout(() => {
          document.getElementById("loading").hidden = true;
      }, 500)
    }
  }
  // FETCH USER DATA

  // FILTER USER DATA
  const filterdata = async() => {
    const response = await fetch(`${url_str}/users`, { 'Content-Type': 'application/json', 'credentials': 'include' });
    const data = await response.json();
    for(const element of data.rows){
      const i = colorOpt.findIndex((el) => el.code === element.user_color)
      if(colorOpt[i] === undefined){
        // Do nothing
      }
      else{
        colorOpt[i]['disabled'] = true
      }
    }
  }
  // FILTER USER DATA 

  // USER DATA STATE
  const [lname, setLname] = useState('')
  const [fname, setFname] = useState('')
  const [mname, setMname] = useState('')
  const [empno, setEmpno] = useState('')
  const [position, setPosition] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [color, setColor] = useState('')
  // USER DATA STATE

  // SUBMIT USER DATA
  const usersubmit = async(e) => {
    e.preventDefault();
    document.getElementById("loading").hidden = false;
    try {
      const response = await fetch(`${url_str}/postuser`, { 
        mode: 'cors',
        method: 'POST',
        headers: headers,
        credentials: 'include',
        body: JSON.stringify({
          lname: lname,
          fname: fname,
          mname: mname,
          empno: empno,
          position: position,
          email: email,
          password: password,
          color: color
        }),
      });
      const data = await response.json();
      if(data && response.status === 200){
        setLname('')
        setFname('')
        setMname('')
        setEmpno('')
        setPosition('')
        setEmail('')
        setPassword('')
        setColor('')
        fetchdata()
      }
    }
    catch (err) {
      console.log(err);
      return alert(`Failed to submit user's information.`)
    }
  }
  // SUBMIT USER DATA
  
  useEffect(() => {
    execSearch()
    filterdata()
  }, [])
  
  return (
    <div className="wrapper">
      <div id='loading'></div>
      <Header />
      <Sidebar />
      <div className="content-wrapper p-2">
        <section className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1>User Information</h1>
              </div>
            </div>
          </div>
        </section>     

        <section className="content">
          <div className="container-fluid row">
            <div className='col-md-3'>
              <div className="card card-default">
                <div className="card-header">
                  <h3 className="card-title">Add New User</h3>
                  <div className="card-tools">
                    <button type="button" className="btn btn-tool" data-card-widget="collapse">
                      <i className="fas fa-minus" />
                    </button>
                    {/* <button type="button" className="btn btn-tool" data-card-widget="remove">
                      <i className="fas fa-times" />
                    </button> */}
                  </div>
                </div>
                
                <div className="card-body">
                  <div className="form-group">
                    <div className="col-md-12">
                      <form onSubmit={usersubmit} autoComplete='off'>
                        <label htmlFor="fname">Lastname</label>
                        <input type="text" value={lname}
                        onChange={(e)=>{setLname(e.target.value)}} required
                        className="form-control" id="lname" placeholder="Last Name" />
                        <br></br>

                        <label htmlFor="username">Firstname</label>
                        <input type="text" value={fname}
                        onChange={(e)=>{setFname(e.target.value)}} required
                        className="form-control" id="fname" placeholder="First Name" />
                        <br></br>
                        
                        <label htmlFor="username">Middlename</label>
                        <input type="text" value={mname}
                        onChange={(e)=>{setMname(e.target.value)}}
                        className="form-control" id="mname" placeholder="Middle Name" />
                        <br></br>

                        <label htmlFor="idnum">Employee Number</label>
                        <input type="text" value={empno}
                        onChange={(e)=>{setEmpno(e.target.value)}} required
                        className="form-control" id="idnum" placeholder="ID Number" />
                        <br></br>

                        <label htmlFor="pos">Position</label>
                        <input type="text" value={position}
                        onChange={(e)=>{setPosition(e.target.value)}} required
                        className="form-control" id="pos" placeholder="Position" />
                        <br></br>

                        <label htmlFor="email">Email Address</label>
                        <input type="email" value={email}
                        onChange={(e)=>{setEmail(e.target.value)}} required
                        className="form-control" id="email" placeholder="Email Address" />
                        <br></br>

                        <label htmlFor="pas">Password</label>
                        <input type="password" value={password}
                        onChange={(e)=>{setPassword(e.target.value)}} required
                        className="form-control" id="pass" placeholder="Password" />
                        <br></br>

                        <label>Pin Color</label>
                        <select value={color}
                        onChange={(e)=>{setColor(e.target.value)}} required
                        className="form-control select2bs4" style={{width: '100%'}}>
                          <option value='' disabled selected> - Choose Pin Color - </option>
                          {colorOpt.map((data, index)=>(
                            <option key={index} value={data.code} hidden={data.disabled}
                            style={{backgroundColor:data.code}}
                            >
                              {data.color}
                            </option>
                          ))}
                        </select>
                        <br></br>

                        <button type="submit" className="btn btn-default float-right">
                          Submit
                        </button>
                      </form>
                    </div>    
                  </div>
                </div>
              </div>
            </div>
              
            <div className='col-md-9'>
              <div className="card card-outline card-primary">
                <div className="card-header">
                  <h3 className="card-title">Information Details</h3>
                  <div className="card-tools">
                    <div className="input-group input-group-sm" style={{width: 180}}>
                      <input type="text" name="table_search" 
                      onChange={(e)=>{setSearch(e.target.value)}}
                      className="form-control float-right" placeholder="Search" />
                      
                      <div className="input-group-append">
                        <button type="button" className="btn btn-default" onClick={execSearch}>
                            <i className="fas fa-search" />
                        </button>
                      </div>
                    </div>  
                  </div>
                </div>

                <div>
                  <table className="table table-head-fixed text-nowrap">
                    <thead>
                      <th>No.</th>
                      <th>Pin Color / Image</th>
                      <th>Name</th>
                      <th>Position</th>
                      <th>ID number</th>
                      <th>Email Address</th>
                      <th>Status</th>
                    </thead>
                    <tbody>
                      {userdata.length != 0
                        ?userdata.map((data, index) => (
                          <tr key={index}>
                            <td className='align-middle'>{index + 1 + tblIndex}</td>
                            <td style={{backgroundColor:data.user_color}}>
                              <img src={data.user_image==='Avatar.png'?'dist/img/Avatar.png':data.user_b64} style={{background:'white'}}
                              className="img-circle elevation-2 img-size-50 align-center" />
                            </td>
                            <td className='align-middle'>{data.user_lname}, {data.user_fname} {data.user_mname} </td>
                            <td className='align-middle'>{data.user_position}</td>
                            <td className='align-middle'>{data.user_empno}</td>
                            <td className='align-middle'>{data.user_email}</td>
                            <td className='align-middle'>{data.user_status}</td>
                          </tr>
                        ))
                        :<tr>
                            <td colSpan={7} className='text-center'><i>No Available Data</i></td>
                        </tr>
                      }
                    </tbody>
                  </table>
                </div>   
                <div className="col-12"
                style={{padding: "10px 20px 10px 20px", 
                display:`${userdata.length != 0?"":"none"}`}}>
                    <Pagination
                    totalPagesOut = {totalPages}
                    rowCountOut = {rowCount}
                    totalrowCount = {totalrowCount}
                    no_of_records_per_page = {no_of_records_per_page}
                    getLoadData={fetchdata}/>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <Footer />
    </div>
  )
}

export default User