import { useState } from 'react'
import { useNavigate } from 'react-router-dom';

const Login = ({ url_str, url_origin }) => {
  const navigate = useNavigate();

  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('Accept', 'application/json');
  headers.append('Origin', `${url_origin}`);
  
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const loginProc = async(e) => {
    e.preventDefault();
    try {
      const response = await fetch(`${url_str}/login`, { 
        mode: 'cors',
        method: 'POST',
        headers: headers,
        credentials: 'include',
        body: JSON.stringify({
          email: username,
          password: password,
        }),
      });
      
      const data = await response.json();
          if(response.status === 200){
            window.setTimeout(() => {
              return navigate('/dashboard');
            }, 500)
          }
          else{
            return alert('Username or Password is incorrect.')
          }
    }
    catch (err) {
      console.log(err);
      return alert('Username or Password is incorrect.');
    }
  }

  return (
  <div className="login-page">
        LOG IN
  <div>
  <meta charSet="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Systems | Log in</title>
  {/* Google Font: Source Sans Pro */}
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback" />
  {/* Font Awesome */}
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css" />
  {/* icheck bootstrap */}
  <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css" />
  {/* Theme style */}
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css" />
  <div className="login-box">
    <div className="login-logo">
      <span href="../../index2.html"><b>Systems </b>COSDD</span>
    </div>
    {/* /.login-logo */}
    <div className="card">
      <div className="card-body login-card-body">
        <p className="login-box-msg">Sign in to update your location</p>
        <form onSubmit={loginProc} autoComplete='off'>
          <div className="input-group mb-3">
            <input type="email" className="form-control" placeholder="Email" required
            value={username}
            onChange={(e)=>{setUsername(e.target.value)}} />
            <div className="input-group-append">
              <div className="input-group-text">
                <span className="fas fa-envelope" />
              </div>
            </div>
          </div>
          <div className="input-group mb-3">
            <input type="password" className="form-control" placeholder="Password" required
            value={password}
            onChange={(e)=>{setPassword(e.target.value)}} />
            <div className="input-group-append">
              <div className="input-group-text">
                <span className="fas fa-lock" />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-8">
              {/* <div className="icheck-primary">
                <input type="checkbox" id="remember" />
                <label htmlFor="remember">
                  Remember Me
                </label>
              </div> */}
            </div>
            {/* /.col */}
            <div className="col-4">
              <button type="submit" className="btn btn-primary btn-block">Sign In</button>
            </div>
            {/* /.col */}
          </div>
        </form>
        <div className="social-auth-links text-center mb-3">
    
         
         
        </div>
        {/* /.social-auth-links */}
        {/* <p className="mb-1">
          <a href="forgot-password.html">I forgot my password</a>
        </p> */}
       
      </div>
      {/* /.login-card-body */}
    </div>
  </div>
  {/* /.login-box */}
  {/* jQuery */}
  {/* Bootstrap 4 */}
  {/* AdminLTE App */}
</div>


        
    </div>
  )
}

export default Login