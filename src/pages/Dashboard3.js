import React from 'react'
import Sidebar from "../components/Sidebar"
import Header from '../components/Header'

const Dashboard = () => {
  return (
    <div>
    <Sidebar/>
    <Header/>
    
    <div className="content-wrapper pl-2 pr-2">
        <section className="content-header">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-6">
                    <h1>DASHBOARD</h1>
                    </div>
                </div>
            </div>
        </section>
      

        {/* <section className="content"> */}
          
            <div className='p-2'>
                <div className='card mt-3'>
                    <div className='card-header'> 
                        DATA TABLE 
                    </div>

                    <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Select2 (Bootstrap4 Theme)</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Minimal</label>
                  <select class="form-control select2bs4" style="width: 100%;">
                    <option selected="selected">Alabama</option>
                    <option>Alaska</option>
                    <option>California</option>
                    <option>Delaware</option>
                    <option>Tennessee</option>
                    <option>Texas</option>
                    <option>Washington</option>
                  </select>
                </div>
             
                <div class="form-group">
                  <label>Disabled</label>
                  <select class="form-control select2bs4" disabled="disabled" style="width: 100%;">
                    <option selected="selected">Alabama</option>
                    <option>Alaska</option>
                    <option>California</option>
                    <option>Delaware</option>
                    <option>Tennessee</option>
                    <option>Texas</option>
                    <option>Washington</option>
                  </select>
                </div>
         
              </div>
             
              <div class="col-md-6">
                <div class="form-group">
                  <label>Multiple</label>
                  <select class="select2bs4" multiple="multiple" data-placeholder="Select a State"
                          style="width: 100%;">
                    <option>Alabama</option>
                    <option>Alaska</option>
                    <option>California</option>
                    <option>Delaware</option>
                    <option>Tennessee</option>
                    <option>Texas</option>
                    <option>Washington</option>
                  </select>
                </div>

                <div class="form-group">
                  <label>Disabled Result</label>
                  <select class="form-control select2bs4" style="width: 100%;">
                    <option selected="selected">Alabama</option>
                    <option>Alaska</option>
                    <option disabled="disabled">California (disabled)</option>
                    <option>Delaware</option>
                    <option>Tennessee</option>
                    <option>Texas</option>
                    <option>Washington</option>
                  </select>
                </div>
            
              </div>
       
            </div>
      
          </div>
 
          <div class="card-footer">
            Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about
            the plugin.
          </div>
      
                    </div>
                    <div className="card">
          <div className="card-header border-transparent">
            <h3 className="card-title">Systems Staff Whereabouts</h3>
            <div className="card-tools">
              <button type="button" className="btn btn-tool" data-card-widget="collapse">
                <i className="fas fa-minus" />
              </button>
              <button type="button" className="btn btn-tool" data-card-widget="remove">
                <i className="fas fa-times" />
              </button>
            </div>
          </div>
          {/* /.card-header */}
          <div className="card-body p-0">
            <div className="table-responsive">
              <table className="table m-0">
                <thead>
                  <tr>
                    <th>Names</th>
                    <th>In the Office</th>
                    <th>Meeting</th>
                    <th>Main Office</th>
                    <th>Official Business</th>
                    <th>Break Time</th>
                    <th>On Leave/Absent</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Claire</a></td>
                  <td></td>
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td><a href="pages/examples/invoice.html">Hara</a></td>
                    <td></td>
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    <td></td>
                  
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Rox</a></td>
                    
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Faith</a></td>
                    
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Well</a></td>
                  <td></td>
                    <td></td>
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    <td></td>
                    <td></td>
                    
                    <td></td>
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Judy</a></td>
                  <td></td>
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                   
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Niel</a></td>
                    
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Pao</a></td>
                    
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Zoren</a></td>
                  <td></td>
                    <td></td>
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Lito</a></td>
                  <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                   
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Lyndon</a></td>
                  <td></td>
                    <td></td>
                    <td></td>
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    <td></td>
                    <td></td>
                   
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Mj</a></td>
                  <td></td>
                    <td></td>
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
            </div>
            {/* /.table-responsive */}
          </div>
          {/* /.card-body */}
          <div className="card-footer clearfix">
            <a href="javascript:void(0)" className="btn btn-sm btn-info float-left">Place New Order</a>
            <a href="javascript:void(0)" className="btn btn-sm btn-secondary float-right">View All Orders</a>
          </div>
          {/* /.card-footer */}
        
                </div>
            </div>
        {/* </section> */}
    </div>
</div>
  )
}

export default Dashboard