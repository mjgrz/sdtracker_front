import { useState, useEffect } from 'react'
import Sidebar from "../components/Sidebar"
import Header from '../components/Header'
import Footer from '../components/Footer'
import absent from '../sounds/absent.mp3'
import breaktime from '../sounds/breaktime.mp3'
import intheoffice from '../sounds/intheoffice.mp3'
import onameeting from '../sounds/onameeting.mp3'
import onbusiness from '../sounds/onbusiness.mp3'
import inthemainoffice from '../sounds/inthemainoffice.mp3'
import { locationOpt } from '../datagrp/DataGrp';
import moment from 'moment';
import './ToolTip.css';

const Dashboard = ({ url_str, url_origin }) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('Accept', 'application/json');
  headers.append('Origin', `${url_origin}`);
  
  // FETCH LOG DATA
  const [logdata, setlogdata] = useState([]);
  const fetchdata = async() => {
    document.getElementById("loading").hidden = false;
    const response = await fetch(`${url_str}/logdashboard/`, { 'Content-Type': 'application/json', 'credentials': 'include' });
    const data = await response.json();
    setlogdata(data.rows);
    console.log(data.rows);
    document.getElementById("loading").hidden = true;
  }
  // FETCH LOG DATA

  // SUBMIT LOCATION
  const [location, setLocation] = useState('');
  const [remarks, setRemarks] = useState('');

  const submitloc = async(e) => {
    e.preventDefault();    
    document.getElementById("loading").hidden = false;
    const datenow = moment(new Date()).format('YYYY-MM-DD');
    const timenow = new Date().toLocaleTimeString();
    try {
      const response = await fetch(`${url_str}/postlog`, { 
        mode: 'cors',
        method: 'POST',
        headers: headers,
        credentials: 'include',
        body: JSON.stringify({
          userid: datasession.userid,
          location: location,
          remarks: remarks,
          date: datenow,
          time: timenow,
        }),
      });
      const data = await response.json();
      if(data && response.status === 200){
        // if(audioplaysubmit(location)) {
          setLocation('')
          setRemarks('')
          fetchdata()
        // }
      }
      else{
        document.getElementById("loading").hidden = true;
      }
    }
    catch (err) {
      console.log(err);
      return alert(`Failed to update location.`);
    }
    document.getElementById("loading").hidden = true;
  }
  // SUBMIT LOCATION

  // PLAY AUDIO
  const audioplay = async(val) => {
    if(val === 'intheoffice'){
      new Audio(intheoffice).play();
    }
    if(val === 'onameeting'){
      new Audio(onameeting).play();
    }
    if(val === 'inthemainoffice'){
      new Audio(inthemainoffice).play();
    }
    if(val === 'onbusiness'){
      new Audio(onbusiness).play();
    }
    if(val === 'breaktime'){
      new Audio(breaktime).play();
    }
    if(val === 'absent'){
      new Audio(absent).play();
    }
  }
  
  const audioplaysubmit = async(val) => {
    if(val === 'In the Office'){
      new Audio(intheoffice).play();
    }
    if(val === 'On a Meeting'){
      new Audio(onameeting).play();
    }
    if(val === 'In the Main Office'){
      new Audio(inthemainoffice).play();
    }
    if(val === 'On an Official Business/Travel'){
      new Audio(onbusiness).play();
    }
    if(val === 'Break Time'){
      new Audio(breaktime).play();
    }
    if(val === 'On Leave/Absent'){
      new Audio(absent).play();
    }
  }
  // PLAY AUDIO
  
  // FETCH DATA SESSION
  const [datasession, setDatasession] = useState([]);
  const fetchsession = (val) => {
    setDatasession(val);
  }
  // FETCH DATA SESSION
  
  useEffect(() => {
    fetchdata()
  }, [])

  return (
  <div className="wrapper">
    <div id='loading'></div>
    <Sidebar/>
    <Header url_str={url_str} fetchsession={fetchsession} />
    <div className="content-wrapper pl-2 pr-2">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-6">
              <h1>DASHBOARD</h1>
            </div>
          </div>
        </div>
      </section>
      
      <section className="content">
        <div className='p-2'>
          <div className='card mt-3'>
            <div className="card-header">
              <h3 className="card-title">Office Monitoring Update</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse">
                  <i className="fas fa-minus" />
                </button>
                {/* <button type="button" className="btn btn-tool" data-card-widget="remove">
                  <i className="fas fa-times" />
                </button> */}
              </div>
            </div>
            
            <div className="card-body">
              <div className="form-group">
                <div className="row">
                  <div className="col-md-12">
                    <form autoComplete='off' onSubmit={submitloc} >
                      <label>Location</label>
                      <select className="form-control select2bs4" 
                      value={location}
                      onChange={(e)=>{setLocation(e.target.value)}}
                      style={{width: '100%'}} required>
                        <option disabled selected value=""> - Choose Location - </option>
                        {locationOpt.map((data, index)=>(
                          <option key={index} value={data.value}>{data.label}</option>
                        ))}
                      </select>
                      
                      <label htmlFor="remark1">Remarks</label>
                      <input type="rem" className="form-control" 
                      id="remark1" placeholder="Remarks"
                      value={remarks}
                      onChange={(e)=>{setRemarks(e.target.value)}} />
                      <br></br>
                      <button type="submit" className="btn btn-default float-right">
                        Submit
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
                  
        <div className="card">
          <div className="card-header border-transparent">
            <h3 className="card-title">Systems Staff Whereabouts</h3>
            <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse">
                  <i className="fas fa-minus" />
                </button>
                {/* <button type="button" className="btn btn-tool" data-card-widget="remove">
                  <i className="fas fa-times" />
                </button> */}
              </div>
            </div>

            <div className="card-body p-0">
              <div className="table-responsive">
                <table className="table m-0 center th">
                  <thead>
                    <tr>
                      <th>Employee</th>
                      <th>In the Office</th>
                      <th>On a Meeting</th>
                      <th>In the Main Office</th>
                      <th>On an Official Business/Travel</th>
                      <th>Break Time</th>
                      <th>On Leave/Absent</th>
                    </tr>
                  </thead>
                  <tbody>
                    {logdata.length != 0
                      ?logdata.map((data, index) => (
                        <tr key={index}>
                          <td className='align-middle' style={{width:'16%'}}>
                            {data.user_lname}, {data.user_fname} {data.user_mname}
                          </td>
                          {data.log_location==='In the Office'?
                          <td className='align-middle' style={{backgroundColor:data.user_color, width:'14%'}} 
                          id='intheoffice'
                          onClick={(e)=>audioplay(e.target.id)} 
                          title={data.log_date+' / '+data.log_time}>
                            <img src={data.user_image==='Avatar.png'?'dist/img/'+data.user_image:data.user_b64} 
                            style={{background:'white', height:'50px'}}
                            className="img-circle elevation-4 img-size-50 align-center" />
                          </td>
                          :<td style={{width:'14%'}}></td>}
                          {data.log_location==='On a Meeting'?
                          <td className='align-middle' style={{backgroundColor:data.user_color, width:'14%'}}
                          id='onameeting' 
                          onClick={(e)=>audioplay(e.target.id)} 
                          title={data.log_date+' / '+data.log_time}>
                            <img src={data.user_image==='Avatar.png'?'dist/img/'+data.user_image:data.user_b64} 
                            style={{background:'white', height:'50px'}}
                            className="img-circle elevation-4 img-size-50 align-center" />
                          </td>
                          :<td style={{width:'14%'}}></td>}
                          {data.log_location==='In the Main Office'?
                          <td className='align-middle' style={{backgroundColor:data.user_color, width:'14%'}}
                          id='inthemainoffice' 
                          onClick={(e)=>audioplay(e.target.id)} 
                          title={data.log_date+' / '+data.log_time}>
                            <img src={data.user_image==='Avatar.png'?'dist/img/'+data.user_image:data.user_b64} 
                            style={{background:'white', height:'50px'}}
                            className="img-circle elevation-4 img-size-50 align-center" />
                          </td>
                          :<td style={{width:'14%'}}></td>}
                          {data.log_location==='On an Official Business/Travel'?
                          <td className='align-middle' style={{backgroundColor:data.user_color, width:'14%'}}
                          id='onbusiness' 
                          onClick={(e)=>audioplay(e.target.id)} 
                          title={data.log_date+' / '+data.log_time}>
                            <img src={data.user_image==='Avatar.png'?'dist/img/'+data.user_image:data.user_b64} 
                            style={{background:'white', height:'50px'}}
                            className="img-circle elevation-4 img-size-50 align-center" />
                          </td>
                          :<td style={{width:'14%'}}></td>}
                          {data.log_location==='Break Time'?
                          <td className='align-middle' style={{backgroundColor:data.user_color, width:'14%'}}
                          id='breaktime' 
                          onClick={(e)=>audioplay(e.target.id)} 
                          title={data.log_date+' / '+data.log_time}>
                            <img src={data.user_image==='Avatar.png'?'dist/img/'+data.user_image:data.user_b64} 
                            style={{background:'white', height:'50px'}}
                            className="img-circle elevation-4 img-size-50 align-center" />
                          </td>
                          :<td style={{width:'14%'}}></td>}
                          {data.log_location==='On Leave/Absent'?
                          <td className='align-middle' style={{backgroundColor:data.user_color, width:'14%'}}
                          id='absent' 
                          onClick={(e)=>audioplay(e.target.id)} 
                          title={data.log_date+' / '+data.log_time}>
                            <img src={data.user_image==='Avatar.png'?'dist/img/'+data.user_image:data.user_b64} 
                            style={{background:'white', height:'50px'}}
                            className="img-circle elevation-4 img-size-50 align-center" />
                          </td>
                          :<td style={{width:'14%'}}></td>}
                        </tr>
                      ))
                      :<tr>
                          <td colSpan={7} className='text-center'><i>No Available Data</i></td>
                      </tr>
                    }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <Footer />
  </div>
  )
}

export default Dashboard 