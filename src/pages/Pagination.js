import React from "react";

const Pagination = ({ totalPagesOut, rowCountOut, getLoadData, totalrowCount, no_of_records_per_page }) => {
  
  const [currentPage, setCurrentPage]=React.useState(1)
  const totalPages = totalPagesOut
  const rowCount = rowCountOut
  const differ = rowCount - 1;
  const one = (currentPage * no_of_records_per_page) - (no_of_records_per_page - 1)
  const two = one + differ
  const pageNumbers = [];

  // start of page conditions
  if(currentPage < 5){
    for (let i = 2; i < totalPages; i++) {
      pageNumbers.push(i);
      if(i >= 5){
        break;
      }
    }
  }
  else {
    for (let ii = currentPage - 2; ii < totalPages; ii++) {
      pageNumbers.push(ii);
      if(ii >= totalPages - 1){
        if(ii >= currentPage + 2){
          break;
        }
        break;
      }
      else{
        if(ii >= currentPage + 2){
          break;
        }
      }
    }
  }
  // end of page conditions

  const page = (number) => {
    // Fetch/Refresh Table;
    getLoadData(number)
    //Set current page
    setCurrentPage(number)
  }

  return (
    <>
      <nav>
          {/* starting page */}
          <ul className="pagination pagination-sm pb-3 pt-2">
            <a className="text-primary" style={{display: `${totalPages <= 1 ? "none": ""}`}}>
              <a
                onClick={() => page(1)}
                data-target="#"
                className={`${currentPage === 1 ? "btn btn-primary" : "btn btn-default"}`}
                style={{width: "auto", minWidth: "28px", borderRadius: "5px", textAlign: "center", padding: "8px", cursor: "pointer"}}
                >
                {1}
              </a>
            </a>&nbsp;
            
            {/* ellipsis */}
            {currentPage >= 5 ? <a style={{width: "28px", textAlign: "center", padding: "8px"}}>...</a> : null}
  
            {/* page between */}
            {pageNumbers.map((number, index) => {
              return <span key={index + 1}>
                <a className="text-primary">
                  <a
                    onClick={() => page(number)}
                    data-target="#"
                    className={`${currentPage === number ? "btn btn-primary" : "btn btn-default"}`}
                    style={{width: "auto", minWidth: "28px", borderRadius: "5px", textAlign: "center", padding: "8px", cursor: "pointer"}}
                    >
                    {number}
                  </a>
                </a>&nbsp;
              </span>
            })}

            
            {/* ellipsis */}
            {currentPage <= totalPages - 4 && totalPages != 6 && totalPages != 5
            ? <a style={{width: "28px", textAlign: "center", padding: "8px"}}>...</a>
            : null}
            {currentPage == 4 && totalPages == 7? <a style={{width: "28px", textAlign: "center", padding: "8px"}}>...</a> : null}
  
            {/* last page */}
            <a className="text-primary" style={{display: `${totalPages <= 1 ? "none": ""}`}}>
              <a onClick={() => page(totalPages)}
                data-target="#"
                className={`${currentPage === totalPages ? "btn btn-primary" : "btn btn-default"}`}
                style={{width: "auto", minWidth: "28px", borderRadius: "5px", textAlign: "center", padding: "8px", cursor: "pointer"}}
                >
                {totalPages}
              </a>
            </a>
          </ul>
      </nav>
  
      {/* page info */}
      <div className="d-flex justify-content-between" style={{display: `${totalPages < 1 ? "none": ""}`}}>
        <span className="">
          <label>{currentPage} of {totalPages} page(s)</label>
        </span>
        <span className="" style={{textAlign: "right"}}>
          <label>
            Showing {one} to {two} out of {totalrowCount} result(s)
          </label>
        </span>
      </div>
    </>
  );  
};

export default Pagination;