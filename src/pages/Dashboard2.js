import React from 'react'
import Sidebar from "../components/Sidebar"
import Header from '../components/Header'

const Dashboard = () => {
  return (
    <div>
    <Sidebar/>
    <Header/>
    
    <div className="content-wrapper pl-2 pr-2">
        <section className="content-header">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-6">
                    <h1>DASHBOARD</h1>
                    </div>
                </div>
            </div>
        </section>
      

        <section className="content">
            <div className='p-2'>
                <div className='card mt-3'>
                    <div className='card-header'> 
                        DATA TABLE 
                    </div>

                    <div className='card-body p-0 table-responsive'>
                        <table className='table table-head-fixed table-hover text-nowrap'>
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Region</th>
                                    <th>District</th>
                                    <th>Project Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>11</td>
                                    <td>Sample District</td>
                                    <td>COSDD Land Project</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                    </div>
                    <div className="card">
          <div className="card-header border-transparent">
            <h3 className="card-title">Systems Staff Whereabouts</h3>
            <div className="card-tools">
              <button type="button" className="btn btn-tool" data-card-widget="collapse">
                <i className="fas fa-minus" />
              </button>
              <button type="button" className="btn btn-tool" data-card-widget="remove">
                <i className="fas fa-times" />
              </button>
            </div>
          </div>
          {/* /.card-header */}
          <div className="card-body p-0">
            <div className="table-responsive">
              <table className="table m-0">
                <thead>
                  <tr>
                    <th>Names</th>
                    <th>In the Office</th>
                    <th>Meeting</th>
                    <th>Main Office</th>
                    <th>Official Business</th>
                    <th>Break Time</th>
                    <th>On Leave/Absent</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Claire</a></td>
                  <td></td>
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td><a href="pages/examples/invoice.html">Hara</a></td>
                    <td></td>
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    <td></td>
                  
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Rox</a></td>
                    
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Faith</a></td>
                    
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Well</a></td>
                  <td></td>
                    <td></td>
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    <td></td>
                    <td></td>
                    
                    <td></td>
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Judy</a></td>
                  <td></td>
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                   
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Niel</a></td>
                    
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Pao</a></td>
                    
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Zoren</a></td>
                  <td></td>
                    <td></td>
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Lito</a></td>
                  <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                   
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Lyndon</a></td>
                  <td></td>
                    <td></td>
                    <td></td>
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    <td></td>
                    <td></td>
                   
                  </tr>
                  <tr>
                  <td><a href="pages/examples/invoice.html">Mj</a></td>
                  <td></td>
                    <td></td>
                    <td> <img src="dist/img/claire.png"  className="img-circle elevation-2 img-size-50" background-color="#cccccc" /></td>
                    
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
            </div>
            {/* /.table-responsive */}
          </div>
          {/* /.card-body */}
          <div className="card-footer clearfix">
            <a href="javascript:void(0)" className="btn btn-sm btn-info float-left">Place New Order</a>
            <a href="javascript:void(0)" className="btn btn-sm btn-secondary float-right">View All Orders</a>
          </div>
          {/* /.card-footer */}
        
                </div>
            </div>
        </section>
    </div>
</div>
  )
}

export default Dashboard