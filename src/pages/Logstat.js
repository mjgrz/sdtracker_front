import { useState, useEffect } from 'react'
import Sidebar from "../components/Sidebar"
import Header from '../components/Header'
import Footer from '../components/Footer'
import Pagination from './Pagination'

const Logstat = ({ url_str, url_origin }) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('Accept', 'application/json');
  headers.append('Origin', `${url_origin}`);
  
  // PAGINATION SETTINGS
  const [totalPages, setTotalPages] = useState(0);
  const no_of_records_per_page = 10;
  const [rowCount, setRowCount] = useState(0);
  const [totalrowCount, setTotalRowCount] = useState(0);
  const [tblIndex, setTblIndex] = useState(0);
  // PAGINATION SETTINGS
  
  // SEARCH
  const [search, setSearch] = useState("");
  // SEARCH

  // FETCH USER DATA
  const execSearch = async() => {
    if(search == ""){
      // document.getElementById("loading").hidden = false;
      const response = await fetch(`${url_str}/logcount`, { 
          mode: "cors",
          method: "POST",
          headers: headers,
          credentials: "include",
          body: JSON.stringify({
          }),
      });
      const data = await response.json();
      setTotalRowCount(data.rows[0].count)
      setTotalPages(Math.ceil(data.rows[0].count / no_of_records_per_page))
      window.setTimeout(() => {
          let number = 1
          fetchdata(number)
      }, 500)
    }
    else if (search != ""){
      // document.getElementById("loading").hidden = false;
      const response = await fetch(`${url_str}/logcountlike`, { 
          mode: "cors",
          method: "POST",
          headers: headers,
          credentials: "include",
          body: JSON.stringify({
              item: search,
          }),
      });
      const data = await response.json();
      setTotalRowCount(data.rows[0].count)
      setTotalPages(Math.ceil(data.rows[0].count / no_of_records_per_page))
      window.setTimeout(() => {
          let number = 1
          fetchdata(number)
      }, 500)
    }
  }
  
  const [logdata, setlogdata] = useState([]);
  const fetchdata = async(number) => {
    if(search == ""){
      const response = await fetch(`${url_str}/logset1`, { 
          mode: "cors",
          method: "POST",
          headers: headers,
          credentials: "include",
          body: JSON.stringify({
              offset: (number - 1) * no_of_records_per_page,
              limit: no_of_records_per_page,
          }),
      });
      const data = await response.json();
      setlogdata(data.rows)
      setRowCount(data.rowCount)
      setTblIndex((number-1)*no_of_records_per_page)
      window.setTimeout(() => {
          // document.getElementById("loading").hidden = true;
      }, 500)
    }
    else if(search != ""){
      const response = await fetch(`${url_str}/logset2`, { 
          mode: "cors",
          method: "POST",
          headers: headers,
          credentials: "include",
          body: JSON.stringify({
              offset: (number - 1) * no_of_records_per_page,
              limit: no_of_records_per_page,
              item: search,
          }),
      });
      const data = await response.json();
      setlogdata(data.rows)
      setRowCount(data.rowCount)
      setTblIndex((number-1)*no_of_records_per_page)
      window.setTimeout(() => {
          // document.getElementById("loading").hidden = true;
      }, 500)
    }
  }
  // FETCH USER DATA

  useEffect(() => {
    execSearch()
  }, [])

  return (
    <div className="wrapper">
      <Sidebar/>
      <Header/>
      
      <div className="content-wrapper pl-2 pr-2">
        <section className="content-header">
          <div className="container-fluid">
            <div className="row">
              <div className="col-sm-6">
                <h1>LOG STATUS</h1>
              </div>
            </div>
          </div>
        </section>
        
        <section className="content">
          <div className="card-body">
            <div className="card">
              <div className="card-header border-transparent">
                <h3 className="card-title">Location Log</h3>
                <div className="card-tools">
                  
                  <div className="input-group input-group-sm" style={{width: 180}}>
                    <input type="text" name="table_search" 
                    onChange={(e)=>{setSearch(e.target.value)}} 
                    className="form-control float-right" placeholder="Search" />
                    
                    <div className="input-group-append">
                      <button type="button" className="btn btn-default" onClick={execSearch}>
                          <i className="fas fa-search" />
                      </button>
                    </div>
                  </div> 
                </div>
              </div>
              
              <div className="card-body p-0">
                <div className="table-responsive">
                  <table className="table m-0">
                    <thead>
                      <th>No.</th>
                      <th>Date / Time</th>
                      <th>Name</th>
                      <th>Location</th>
                      <th>Remarks</th>
                    </thead>
                    <tbody>
                      {logdata.length != 0
                        ?logdata.map((data, index) => (
                          <tr key={index}>
                            <td className='align-middle'>{index + 1 + tblIndex}</td>
                            <td className='align-middle'>{new Date(data.log_date).toDateString()} / {data.log_time}</td>
                            <td className='align-middle'>{data.user_lname}, {data.user_fname} {data.user_mname}</td>
                            <td className='align-middle'>{data.log_location}</td>
                            <td className='align-middle'>
                              {data.log_remarks!=''&&data.log_remarks!=null?data.log_remarks:<i>Not Available</i>}
                            </td>
                          </tr>
                        ))
                        :<tr>
                            <td colSpan={5} className='text-center'><i>No Available Data</i></td>
                        </tr>
                      }
                    </tbody>
                  </table>
                </div>
                <div className="col-12"
                style={{padding: "10px 20px 10px 20px", 
                display:`${logdata.length != 0?"":"none"}`}}>
                    <Pagination
                    totalPagesOut = {totalPages}
                    rowCountOut = {rowCount}
                    totalrowCount = {totalrowCount}
                    no_of_records_per_page = {no_of_records_per_page}
                    getLoadData={fetchdata}/>
                </div>
              </div>

            </div>
          </div>
        </section>
      </div>
      <Footer />
    </div>
  )
}
 
export default Logstat