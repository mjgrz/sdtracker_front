import { useState, useEffect } from 'react'
import Sidebar from "../components/Sidebar"
import Header from '../components/Header'
import Footer from '../components/Footer'
import { colorOpt } from '../datagrp/DataGrp';

const Profile = ({ url_str, url_origin }) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('Accept', 'application/json');
  headers.append('Origin', `${url_origin}`);

  // SUBMIT PROFILE
  const [useremail, setUseremail] = useState('');
  const [password, setPassword] = useState('');
  const [picture, setPicture] = useState('');
  const [base64, setBase64] = useState('');
  const [color, setColor] = useState('');

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();

    reader.onloadend = () => {
      setBase64(reader.result);
    };

    if (file) {
      reader.readAsDataURL(file);
      setPicture(file.name)
    }
  };

  const loadUser = async() => {
    document.getElementById("loading").hidden = false;
    const response = await fetch(`${url_str}/users`, { 'Content-Type': 'application/json', 'credentials': 'include' });
    const data = await response.json();
    for(const element of data.rows){
      const i = colorOpt.findIndex((el) => el.code === element.user_color)
      if(colorOpt[i] === undefined){
        // Do nothing
      }
      else{
        colorOpt[i]['disabled'] = true
      }
    }
    document.getElementById("loading").hidden = true;
  }

  const submitprofile = async(e) => {
    e.preventDefault();
    document.getElementById("loading").hidden = false;
    try {
      const response = await fetch(`${url_str}/postprofile`, { 
        mode: 'cors',
        method: 'POST',
        headers: headers,
        credentials: 'include',
        body: JSON.stringify({
          userid: datasession.userid,
          email: useremail,
          password: password,
          picture: picture,
          base64: base64,
          color: color,
        }),
      });
      const data = await response.json();
      if(data && response.status === 200){
        // Do Nothing
      }
    }
    catch (err) {
      console.log(err);
      return alert(`Failed to update profile.`)
    }
    document.getElementById("loading").hidden = true;
  }
  // SUBMIT PROFILE

  // FETCH DATA SESSION
  const [datasession, setDatasession] = useState([]);
  const fetchsession = (val) => {
    setDatasession(val);
    setUseremail(val.email);
    setPassword(val.password);
    setBase64(val.base64)
    setPicture(val.image)
    setColor(val.color);
  }
  // FETCH DATA SESSION
  
  useEffect(() => {
    loadUser()
  }, [])

  return (
    <div className="wrapper">
      <div id='loading'></div>
      <Sidebar/>
      <Header url_str={url_str} fetchsession={fetchsession} />
      
      <div className="content-wrapper pl-2 pr-2">
          <section className="content-header">
              <div className="container-fluid">
                  <div className="row-mb-2">
                      <div className="col-sm-6">
                      <h1>Profile</h1>
                      </div>
                  </div>
              </div>
          </section>
        <section className="content">
          <div className="container-fluid row">
            <div className="col-md-3 float-left">
              <div className="card card-primary card-outline">
                <div className="card-body box-profile">
                  <div className="text-center">
                    <img className="profile-user-img img-fluid img-circle" 
                    src={picture==='Avatar.png'?'dist/img/Avatar.png':datasession.base64} alt="User profile picture" />
                  </div>
                  <h3 className="profile-username text-center">{datasession.fullname}</h3>
                  <p className="text-muted text-center">{datasession.position}</p>
                  <ul className="list-group list-group-unbordered mb-3">
                    <li className="list-group-item">
                      <b>ID Number</b> <span className="float-right text-primary">{datasession.empno}</span>
                    </li>
                    <li className="list-group-item">
                      <b>Email</b> <span className="float-right text-primary">{useremail}</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-md-9 float-right">
              <div className="card card-primary">
                <div className="card-header p-2">
                  <h3 className="card-title">Edit Profile</h3>
                </div>
                <form autoComplete='off' onSubmit={submitprofile}>
                  <div className="card-body">
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Email address</label>
                        <input type="email" required value={useremail}
                        onChange={(e)=>{setUseremail(e.target.value)}}
                        className="form-control" id="exampleInputEmail1" placeholder="Enter email" />
                    </div>
                    <div className="form-group">
                      <label htmlFor="exampleInputPassword1">Password</label>
                      <input type="password" required value={password}
                      onChange={(e)=>{setPassword(e.target.value)}}
                      className="form-control" id="exampleInputPassword1" placeholder="Password" />
                    </div>
                    <div className="form-group">
                      <label htmlFor="exampleInputFile">Change or Upload Avatar</label>
                      <div className="input-group">
                        <div className="custom-file">
                          <input type="file"
                          onChange={(e)=>{handleFileChange(e)}}
                          className="custom-file-input" id="exampleInputFile" />
                          <label className="custom-file-label" htmlFor="exampleInputFile">{picture?picture:'Choose File'}</label>
                        </div>
                        {/* <div className="input-group-append">
                          <span className="input-group-text">Upload</span>
                        </div> */}
                      </div>
                    </div>
                    <div className="row">
                      <div className="col">
                        <label>Pin Color</label>
                        <section>
                          <select required value={color}
                          onChange={(e)=>{setColor(e.target.value)}}
                          className="form-control select2bs4" style={{width: '100%'}}>
                            <option value='' disabled selected> - Choose Pin Color - </option>
                            {colorOpt.map((data, index)=>(
                              <option key={index} value={data.code} hidden={data.disabled} style={{backgroundColor:data.code}}>
                                <div> {data.color} </div>
                              </option>
                            ))}
                          </select>
                        </section>
                      </div>
                    </div>
                  </div>
                  <div className="card-footer">
                    <button type="submit" className="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
      <Footer />
    </div>
  )
}

export default Profile