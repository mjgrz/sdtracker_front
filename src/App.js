import { useState } from 'react'
import {HashRouter, Route, Routes} from "react-router-dom";
import Protected from "./Protected.js";
import Login from "./pages/Login";
import User from "./pages/User";
import Dashboard from "./pages/Dashboard";
import Logstat from "./pages/Logstat";
import Profile from "./pages/Profile";


function App() {
  const url_origin = "http://localhost:3000";
  const url_str = "http://localhost:4000/sdt";

  return (
    <HashRouter>
    <Routes>
      <Route>
        <Route path="/" element = {<Login url_str={url_str} url_origin={url_origin} />}/>
        <Route path="/user" element = {
          <Protected url_str={url_str}>
            <User url_str={url_str} url_origin={url_origin} />
          </Protected>}
        />
        <Route path="/dashboard" element = {
          <Protected url_str={url_str}>
            <Dashboard url_str={url_str} url_origin={url_origin} />
          </Protected>}
        />
        <Route path="/logstat" element = {
          <Protected url_str={url_str}>
            <Logstat url_str={url_str} url_origin={url_origin} />
          </Protected>}
        />
        <Route path="/profile" element = {
          <Protected url_str={url_str}>
            <Profile url_str={url_str} url_origin={url_origin} />
          </Protected>}
        />
      </Route>
    </Routes>
    </HashRouter>
  );
}

export default App;
