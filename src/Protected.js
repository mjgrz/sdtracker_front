import React from "react";
import { useNavigate } from "react-router-dom";

const Protected = ({ children, url_str }) => {
const [isLoggedIn, setIsLoggedIn] = React.useState();
const navigate = useNavigate();

  fetch(`${url_str}/checksession`,{credentials:"include"})
  .then((response) => response.json())
  .then((responseJson) => {
    const res = responseJson;
    if (res.response === "notLoggedIn") {
      return setIsLoggedIn(false),
      console.log(res.response);
    } 
    else {
      return setIsLoggedIn(true),
      console.log(res.response);
    }

  })
  .catch((error) => {
    console.log("Session not Established...");
    return setIsLoggedIn(false);
  });

  if (isLoggedIn === true) {
    return children;
  }
  else if (isLoggedIn === false) {
    return  navigate("/");
  }

};

export default Protected;