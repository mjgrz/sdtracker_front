const colorOpt = [
    {id:1, code:"#caffbf", rgba:"rgba(202, 255, 191, 1)", color:"Tea Green", disabled: false},
    {id:2, code:"#fdffb6", rgba:"rgba(253, 255, 182, 1)", color:"Cream", disabled: false},
    {id:3, code:"#ffd6a5", rgba:"rgba(255, 214, 165, 1)", color:"Sunset", disabled: false},
    {id:4, code:"#ffadad", rgba:"rgba(255, 173, 173, 1)", color:"Melon", disabled: false},
    {id:5, code:"#b1e6f3", rgba:"rgba(177, 230, 243, 1)", color:"Non Photo Blue", disabled: false},
    {id:6, code:"#f7adc3", rgba:"rgba(247, 173, 195, 1)", color:"Cherry Blossom Pink", disabled: false},
    {id:7, code:"#d8bbff", rgba:"rgba(216, 187, 255, 1)", color:"Mauve", disabled: false},
    {id:8, code:"#9fafff", rgba:"rgba(159, 175, 255, 1)", color:"Jordy Blue", disabled: false},
    {id:9, code:"#b3b3a8", rgba:"rgba(179, 179, 168, 1)", color:"Ash Gray", disabled: false},
    {id:10, code:"#dcbcaa", rgba:"rgba(220, 188, 170, 1)", color:"Desert Sand", disabled: false},
    {id:11, code:"#f2e0b3", rgba:"rgba(242, 224, 179, 1)", color:"Wheat", disabled: false},
    {id:12, code:"#fcefb4", rgba:"rgba(252, 239, 180, 1)", color:"Vanilla", disabled: false},
    {id:13, code:"#a4bd84", rgba:"rgba(164, 189, 132, 1)", color:"Olivine", disabled: false},
    {id:14, code:"#b17c82", rgba:"rgba(177, 124, 130, 1)", color:"Old Rose", disabled: false},
    {id:15, code:"#279e9d", rgba:"rgba(39, 158, 157, 1)", color:"Light Sea Green", disabled: false},
    {id:16, code:"#75dad7", rgba:"rgba(117, 218, 215, 1)", color:"Tiffany Blue", disabled: false},
    {id:17, code:"#e7c6a2", rgba:"rgba(231, 198, 162, 1)", color:"Desert Sand", disabled: false},
    {id:18, code:"#bf9675", rgba:"rgba(191, 150, 117, 1)", color:"Lion", disabled: false},
    {id:19, code:"#ee7272", rgba:"rgba(238, 114, 114, 1)", color:"Light Coral", disabled: false},
    {id:20, code:"#938fb8", rgba:"rgba(147, 143, 184, 1)", color:"Cool Gray", disabled: false},
];

const locationOpt = [
    {id:1, value:"In the Office", label:"In the Office"},
    {id:2, value:"On a Meeting", label:"On a Meeting"},
    {id:3, value:"In the Main Office", label:"In the Main Office"},
    {id:4, value:"On an Official Business/Travel", label:"On an Official Business/Travel"},
    {id:5, value:"Break Time", label:"Break Time"},
    {id:6, value:"On Leave/Absent", label:"On Leave/Absent"},
];

export {
    colorOpt,
    locationOpt
};
